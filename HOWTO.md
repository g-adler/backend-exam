# Como inicializar a aplicação

### Primeiro damos permissão de execução para o arquivo start.sh

> chmod +x start.sh

### E então executamos o start.sh e o script cuidará do resto
> ./start.sh

### Após isso a aplicação roda automaticamente
### Para subir novamente ative o ambiente de desenvolvimento
> source backend/bin/activate

### E rode o Flask
> flask run
