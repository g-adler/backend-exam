import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)

from app.db import get_db

bp = Blueprint('tasks', __name__)

@bp.route('/tasks', methods= ('GET', 'POST'))
def ListTasks():
    if request.method == 'GET':
        db = get_db()
        tasks = db.execute(
            'SELECT task.id, task.user_id, task.description, user.name '
            'FROM task '
            'INNER JOIN user '
            'ON user.id = task.user_id'
            ).fetchall()
        return render_template('Tasks.html', tasks=tasks)

    if request.method == 'POST':

        user_id = request.form['user_id']
        description  = request.form['description']
        db = get_db()
        error = None
        error = ValidateTask(user_id,description,db)
        if not error:
            db.execute(
                'INSERT INTO task ( user_id, description) VALUES (?, ?)',
                (user_id,description)
            )
            db.commit()
            return redirect(url_for('tasks.ListTasks'))

        else: 
           flash (error)
    return redirect(url_for('tasks.ListTasks'))

@bp.route('/tasks/<int:id>', methods=('GET', 'PUT', 'DELETE'))
def EditTask(id):
    db = get_db()
    task = db.execute(
        'SELECT id, user_id, description'
        ' FROM task where id = (?)',(str(id))
        ).fetchall()
    return render_template('EditTasks.html', task=task)

        

def ValidateTask ( user_id, description, db ):
    error = ()

    if (len(description) > 50) :
        error = error + ('Descrição deve ter menos de 50 letras.',)
    if    db.execute(
        'SELECT id FROM user WHERE id = ?', (user_id,)
        ).fetchone() is None:
        error = error + (
            'Usuário não cadastrado.',)

    return error
   
