import functools

import DNS

from validate_email import validate_email

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)

from app.db import get_db

bp = Blueprint('users', __name__)

@bp.route('/users', methods= ('GET', 'POST'))
def ListUsers():
    if request.method == 'GET':
        db = get_db()
        users = db.execute(
            'SELECT id, name, age, user_type, email'
            ' FROM user',
            ).fetchall()
        return render_template('Users.html', users=users)

    if request.method == 'POST':

        name = request.form['name']
        age  = request.form['age']
        email  = request.form['email']
        user_type  = request.form['user_type']
        db = get_db()
        error = None
        error = ValidateUser(name, age, email, user_type, db)
        if not error:
            db.execute(
                'INSERT INTO user (name, age, email, user_type) VALUES (?, ?, ?, ?)',
                (name, age, email, user_type)
            )
            db.commit()
            return redirect(url_for('users.ListUsers'))

        flash(error)
    return redirect(url_for('users.ListUsers'))

@bp.route('/users/<int:id>', methods=('GET', 'PUT', 'DELETE'))
def EditUser(id):
    db = get_db()
    user = db.execute(
        'SELECT id, name, age, user_type, email'
        ' FROM user where id = (?)',(str(id))
        ).fetchall()
    return render_template('EditUsers.html', user=user)

        

def ValidateUser ( name, age, email, user_type, db ):
    error = ()

    if (len(name) > 50) :
        error = error + ('Nome deve ter menos de 50 letras.',)
    if (int(age) < 18): 
        error = error + ('Usuário deve ser maior de 18',)
    if ((validate_email("{}".format(email)) is False)
           or email is None):
        error = error + ('Email Inválido .',)
    if user_type is None:
        error = error + ('Tipo de Usuário inválido',)    

    if    db.execute(
        'SELECT id FROM user WHERE name = ?', (name,)
        ).fetchone() is not None:
        error = error + (
            'Usuário {} já é cadastrado.'.format(name),)

    return error
   
