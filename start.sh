#!/bin/bash

echo 'Criando ambiente virtual usando Python 3.x'

python3 -m venv  'backend'

echo 'Ativando ambiente virtual'

source backend/bin/activate

echo "Instalando dependências e setando variaveis de ambiente"

export FLASK_APP='app'
export FLASK_ENV='dev'

pip install flask
pip install validate_email
pip install py3dns

echo 'Inicializando o banco de Dados'

flask init-db

echo 'Subindo servidor'

flask run
